namespace SampleApp
{
    using System;
    using Windows.ApplicationModel.Resources;
    using Windows.UI.Popups;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Navigation;

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private static MainPage _current;
        private static readonly ResourceLoader _resourceLoader = new ResourceLoader();

        public MainPage()
        {
            InitializeComponent();
            _current = this;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        #region Settings

        /// <summary>
        /// Opens the command settings flyout.
        /// </summary>
        /// <param name="command">The settings command issued by the user.</param>
        internal static void OpenCommandSettingsFlyout(IUICommand command)
        {
            _current.CommandSettingsFlyout.Open();
        }

        /// <summary>
        /// Handles the Click event of the Commit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private async void Commit_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Commit settings here instead of showing this dialog
            MessageDialog confirmationDialog = new MessageDialog(
                string.Format(_resourceLoader.GetString("ConfirmationDialogContentFormat"), Setting.Text),
                _resourceLoader.GetString("ConfirmationDialogTitle"));

            await confirmationDialog.ShowAsync();
        }

        #endregion
    }
}
